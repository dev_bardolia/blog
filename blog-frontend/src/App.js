import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Container, Row, Col, Button, Navbar, Nav, Breadcrumb, Stack, Alert, Form, InputGroup, Badge, Card, Carousel} from "react-bootstrap";
import img2 from './images/img2.png';

function App() {
  return (
    <div>
    <Container>
      <Navbar sticky="top" bg="light" style={{borderRadius: 10, marginTop:20}} >
      <Container>
          <Navbar.Brand>
            <h4>Therapy Connect</h4> 
          </Navbar.Brand>
          <Nav>
            <Nav.Link>
              Medicine
            </Nav.Link>
            <Nav.Link>
              Diagnostics
            </Nav.Link>
            <Button variant="success" style={{marginLeft:10}}>
              Login / Sign up
            </Button>
          </Nav>
        </Container>
      </Navbar>
      <br/>
      <Row>
        <Breadcrumb style={{width: "100%"}}>
            <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
            <Breadcrumb.Item href="#">Blogs</Breadcrumb.Item>
            <Breadcrumb.Item active>Is chronic fatigue syndrome making you a zombie?</Breadcrumb.Item>          
        </Breadcrumb>
      </Row>
      <br/>
      <Row >
        <div style={{backgroundImage:`url(${img2})`, height:500, borderRadius: 20}}>
        {/* <img src={require("./img2.png")} style={{height:400, width:'100%'}} /> */}
        <Container>
          <Row>
            <Col md={{ span: 6}} style={{marginLeft: 70}}>
              <div style={{backgroundColor: '#fff',  marginTop: 400, borderRadius: 15, }}>
                {/* <br/> */}
                <div style={{padding: 30}}>
                <h1>Is chronic fatigue syndrome making you a zombie?</h1>
                <h4>Komal Patel</h4>
                <Stack gap={4}>
                <h5>Homeopethic</h5>
                <h6>March 05, 2022</h6>
                <p>While Chronic Fatigue Syndrome has no known cure, the right kind of diet can work wonders in keeping your energy 
                  levels up. CFS is easy condition to live with. However, with a few simple lifestyle and diet changes suggested here, 
                  there's no reason why someone with CFS can't be in control of the disease, rather than letting it control everything 
                  they do in their life.</p>
                <p>With our list of food dos and don'ts, keeping the symptoms of CFS in check will soon be a cinch.</p>
                <h5>Curb the carbs</h5>
                <p>Try and follow a low carb diet as much as possible. Cut out foods that contain processed and or refined carbs such 
                  as white bread, crackers, cookies, cakes, and soft drinks. Replace these with fresh fruits and vegetables to 
                  immediately feel more energetic. Avoid sugar and artificial sweeteners as they initially cause a rapid rise in blood
                   sugar, followed by a sharp dip. This roller coaster causes fatigue and anxiety, leading to further sugar cravings. 
                   Soon, you'll be on this vicious cycle that leaves you totally drained.</p>
                <img src={require("./images/img3.jpg")} style={{ borderRadius: 20}} />
                <h5>The heart of the matter</h5>
                <p>Steer clear of foods that are are deep-fried, have high saturated fat content or excessive salt. These put you 
                  in the high-risk zone of developing cardiovascular diseases. However, do include healthy fats such as extra virgin
                   olive oil in your diet to boost your immune function, correct hormonal imbalances, and also help with cognitive 
                   function. What's more, they're also an excellent source of energy!</p>
                <h5>Protein power</h5>
                <p>Give dairy products such as milk and cheese a miss and switch to animal protein found in white meat, fish, and 
                  eggs. These are excellent sources of several essential amino acids that your body needs to function smoothly. 
                  Snacking on nuts and seeds in between meals is also a good way to get your protein fix.</p>
                <h5><Badge bg="secondary" style={{marginRight:10}}>Everyday fitness</Badge>
                <Badge bg="secondary" style={{marginRight:10}}>Obesity</Badge>
                <Badge bg="secondary" style={{marginRight:10}}>Alternative Medicine</Badge>
                <Badge bg="secondary" style={{marginRight:10}}>Ayurveda</Badge>
                <Badge bg="secondary" style={{marginTop: 10, marginRight:10}}>Digestion</Badge></h5>
                
                </Stack>
                </div>
              </div>
            </Col>
            <Col md={{ offset:1, span: 4 }} style={{marginTop:550}}>
              <Stack gap={4}>
                <Alert variant="success">
                  <Container>
                    <Row>
                      <Col xs={8}><Alert.Heading>FREE consultation</Alert.Heading>
                            <p>with Therapy connect doctors.</p>
                            <Button variant="link" style={{padding:0}}>Read more</Button>
                      </Col>           
                      <Col xs={4}><img src="" style={{height:75, width:75}}/></Col>         
                    </Row>
                  </Container>
                </Alert>
                <h6>POPULAR ARTICALS</h6>
                <Container>
                  <Row>
                    <Col xs={4}><img src="" /></Col>                    
                    <Col><h6>Obesity - Women and ayurveda</h6>
                    <p>Obesity is the condition in which...</p></Col>
                  </Row>
                  <Row>
                    <Col xs={4}><img src="" /></Col>                    
                    <Col><h6>The best and worst foods to cure a hangover</h6>
                    <p>Healthy nutrition consists of...</p></Col>
                  </Row>
                  <Row>
                    <Col xs={4}><img src="" /></Col>                    
                    <Col><h6>Water - A forgotten nutrient</h6>
                    <p>The science of hanovers...</p></Col>
                  </Row>
                </Container>
                <Alert variant="secondary">
                  <div style={{margin:15}}>
                  <img src="" style={{height:100, width: 100}} />
                  <Alert.Heading>Stay connected with us</Alert.Heading> 
                  <InputGroup style={{marginTop:20}}>
                  <Form.Control type="email" placeholder="Your email address" />                  
                    <Button variant="warning" >
                      Submit
                    </Button>
                  </InputGroup>
                  </div>
                </Alert>
                <Stack gap={4}>
                  <h6>RELATED QUESTIONS</h6>
                  <div>                  
                    <h5>Ayurveda basic</h5>
                    <p>I want to know something from ayurveda. What or who taught ayurveda in beginning...</p>
                    <a href="">Read more</a>                
                  </div> 
                  <hr/> 
                  <div>
                    <h5>Obesity problem</h5>
                    <p>I'm 42 now and I have obesity problem. Are there any ayurvedic treatments for ob...</p>
                    <a href="">Read more</a>
                  </div>    
                  <hr/> 
                  <div>
                    <h5>Ayerveda suggestion on foods to avoid</h5>
                    <p>Can ayurveda doctor recommend what should a person eat and what they should not...</p>
                    <a href="">Read more</a>
                  </div>  
                  <hr/>                
                </Stack>
              </Stack>
            </Col>
          </Row>
          <Row style={{marginLeft:85, marginTop:50}}>
        <Col><h3>Recommended for you</h3> 
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do Eiusmod.</p>
          </Col>               
      </Row>
            <Row style={{marginLeft:30, marginTop:20}}>
            <Col>
                <Card style={{width:350}} border="light">
                  <Card.Img variant="top" src="https://placeimg.com/300/120/any"  />
                  <Card.Body>
                    <Card.Title>Is chronic fatigue syndrome making you a zombie?</Card.Title>
                    <Card.Text>While Chronic Fatigue Syndrome has no known cure, the right kind of diet can work wonders 
                    in keeping your energy </Card.Text>  
                    <Badge bg="secondary">Everyday fitness</Badge>
                  </Card.Body>                
                </Card>
              </Col>   
              <Col>
              <Card style={{width:350}} border="light">
                  <Card.Img variant="top" src="https://placeimg.com/300/120/any"/>
                  <Card.Body>
                    <Card.Title>Obesity - Women and ayurveda</Card.Title>
                    <Card.Text>Obesity is the condition in which body fat is accumulated in excess quantity. It may create a 
                      negative impact on... </Card.Text>  
                      <Badge bg="secondary" style={{marginRight:5}}>Obesity</Badge>
                      <Badge bg="secondary" style={{marginRight:5}}>Ayurveda</Badge>
                      <Badge bg="secondary" style={{marginRight:5}}>Digestion</Badge>
                      <Badge bg="secondary" style={{marginRight:5}}>Alternative Medicine</Badge>
                  </Card.Body>                
                </Card>
              </Col>              
              <Col>
              <Card style={{width:350}} border="light">
                  <Card.Img variant="top" src="https://placeimg.com/300/120/any"/>
                  <Card.Body>
                    <Card.Title>The best and worst foods to cure a hangover</Card.Title>
                    <Card.Text>The science of hanovers (& how to prevent them) is largerly unstudied, which is why people invent 
                      their own cures. Though no one meal or drink can cure a hangover, certain foods are better for refueling than others.
                      You should restock your body with... </Card.Text>  
                  </Card.Body>                            
                </Card>
              </Col>   
              <Button variant="link" style={{marginBottom:70, marginTop:30}}>
                More articles
              </Button>  
            </Row>        
            <Alert variant="success">
              <Stack direction="horizontal">
                <div style={{margin:90, color:'#000'}} >
                  <h1>Professional care for your family</h1><br/>
                  <p>Need professional help? Our support staff will answer your questions. Download our app now make an appointment.</p><br/>
                  <a href="https://www.apple.com/in/app-store/"><img src={require("./images/5a902db97f96951c82922874.png")} style={{height:45}}/></a>
                  <a href="https://play.google.com/store"><img src={require("./images/5a902dbf7f96951c82922875.png")} style={{height:65}} /></a>
                </div>
              <img src="" style={{height:400, width:400, marginLeft:70, marginRight:70}}/>
              </Stack>              
            </Alert>
            <Row style={{marginTop:100}}>
              <Col><h5>Company</h5><br/>
              <p>About us</p>
              <p>Blog</p>
              <p>careers</p>
              <p>Press info</p>
              <p>Contact us</p>
              </Col>
              <Col>
                <h5>For patients</h5>     <br/>             
                <p>Serach for doctors</p>
                <p>Serach for clinics</p>
                <p>Serach for hospitals</p>
                <p>Book diagnostic test</p>
                <p>Book full body checkups</p>
                <p>Covid hosptial listing</p>
                <p>Read health articals</p>
                <p>Read about medicines</p>
                <p>Health app</p>
              </Col>
              <Col>
              <h5>For doctors</h5>  <br/>
              <p>Therapy profile</p> <br/>
              <h5>For clinics</h5><br/>
              <p>Ray by therapy</p>
              <p>Therapy reach</p>
              <p>Ray tab</p>
              <p>Therapy pro</p>
              </Col>
              <Col>
              <h5>For hospitals</h5>  <br/>
              <p>Insta by therapy</p>
              <p>Qikewell by therapy</p>
              <p>Therapy profile</p>
              <p>Therapy reach</p>
              <p>Therapy drive</p>  <br/>
              <h5>For corporates</h5>  <br/>
              <p>Wellness plans</p>
              </Col>
              <Col>
                <h5>More links</h5><br/>
                <p>Developers</p>
                <p>Privacy policy</p>
                <p>Terms & conditions</p>
                <p>Therapy connect wiki</p>
              </Col>
              <Col>
                <h5>Social media</h5>
              </Col>
            </Row>
            <hr/>
            <h3>Therapy connect</h3>
        </Container>
        </div>
      </Row>
      
      </Container>      
    </div>
  );
}

export default App;
